# DEPRECATED

Moved to operations wiki

Keeping the repo for mkdocs + gitlab pages + pipeline reference

# infra-docs

## Adding documentation

Create a Markdown formatted file (`*.md`) in the docs directory. Add an entry to the `nav` list in the mkdocs.yml file.
