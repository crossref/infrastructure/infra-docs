# Running Applications at Crossref

WIP

## High Level Design

The application should mostly follow the [12 Factor App](https://12factor.net/) method. More specific notes and requirements are detailed below.


## Detailed Design

The code required to run the application must be stored in a git repository in our GitLab organisation at <https://gitlab.com/crossref>. The git repo should only contain text files and not images or binaries. Images and binaries should be stored on more appropriate storage like filesystem/disk or S3.

If code dependencies differ between local development and production then you could make separate dependency files with clear names denoting their separate uses.

The application repo should have a Dockerfile which contains all of the steps required to build a container image. This image build should be self-contained so that the application can be built and run independently. For larger projects it may give better performance by using caching to do a compilation/generation step separately either in the build pipeline or on the developer machine and pass the build artifacts into the container image build process. Though it should still be possible to build the container image without being reliant on the pipeline.

The application should run as a container on Fargate. This allows us to have rolling zero-downtime deployments and automatic rollback if health checks fail.

The application should be stateless such that it can run inside a container and no data or work is lost when the container is replaced.

The application should have some form of health check. This is used by the load balancer to periodically ensure that a given container backend is still working and should be receiving requests. It is also used by the Fargate rolling deploy mechanism to detect when the new application container startup has completed. The health check should be a path like `/health-check` which returns a 200 response. The application should not perform other functions like connecting to a database in order to serve the health check response. It should purely be to detect that the application has started and is able to receive and respond to HTTP requests.

We should know how to best scale the application containers. If CPU is the bottleneck, we can scale based on CPU utilization. If the number of worker processes is the bottleneck, we can scale based on the number of requests we receive.

If the application is a web app, it should be protected by a load balancer so that we can scale up the number of application containers without changing domains/URLs.

If the application is event driven, it should scale based on the number of events left in the queue. 

If the application needs to connect to a database or other external service, care should be taken to ensure that the application scaling up too much doesnt have knock on effects to other services. The scaling should have an appropriate minimum and maximum number of containers. The maximum should be set such that it can handle unexpected spikes in load without being so high that a very large spike causes us to spend lots of money servicing unnecessary requests. In these cases we have better methods of dealing with the traffic and it would be better to have the application be slower/down temporarily while we implement those methods.

Application logs should be sent to stdout which is then collected by CloudWatch automatically.

The application should be capable of running in different environments so that we can test code and deployment changes without affecting a production service.
